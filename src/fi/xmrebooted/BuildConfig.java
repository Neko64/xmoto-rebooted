package fi.xmrebooted;

public class BuildConfig {
	
	// Java, Y U no have a preprocessor?! ;_;
	
	public static final boolean kDEBUG_LOG = true;
	
	// Useful info regarding software versioning: http://semver.org/
	public static final int kVERSION_MAJOR = 0;
	public static final int kVERSION_MINOR = 0;
	public static final int kVERSION_PATCH = 1;
}
