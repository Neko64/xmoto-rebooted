package fi.xmrebooted;

import fi.xmrebooted.xmoto.Game;

public class Main {
	
	public static void main(String[] args) {
		Game game = new Game();
		
		/* Do some other stuff here */
		
		game.Run();
	}
}
