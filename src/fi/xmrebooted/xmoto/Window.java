package fi.xmrebooted.xmoto;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.Callbacks.*;

import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.nio.IntBuffer;

import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.system.MemoryStack;

public class Window {
	
	/*
	 * TODO: Move this class to some other package
	 * */
	
	private long m_wndHandle;
	
	public Window() {
		m_wndHandle = NULL;
	}
	
	public boolean Close() {
		glfwFreeCallbacks(m_wndHandle);
		glfwDestroyWindow(m_wndHandle);
		
		return true;
	}
	
	public boolean Create(int width, int height, String title) {
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
		
		m_wndHandle = glfwCreateWindow(width, height, title, NULL, NULL);
		
		if (NULL == m_wndHandle) {
			throw new RuntimeException("Could not create GLFW window");
		}
		
		try (MemoryStack s = stackPush()) {
			IntBuffer w = s.mallocInt(1);
			IntBuffer h = s.mallocInt(1);
			
			glfwGetWindowSize(m_wndHandle, w, h);
			GLFWVidMode videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
			glfwSetWindowPos(m_wndHandle, 
				(videoMode.width() - w.get(0)) / 2,
				(videoMode.height() - h.get(0)) / 2
			);
		}
		
		glfwMakeContextCurrent(m_wndHandle);
		glfwSwapInterval(1);
		
		glfwShowWindow(m_wndHandle);
		
		return true;
	}
	
	public void SwapBuffers() {
		glfwSwapBuffers(m_wndHandle);
	}
	
	
	// TODO: Make these kinds of functions have public visibility
	public boolean IsOpen() { assert(NULL != m_wndHandle); return !glfwWindowShouldClose(m_wndHandle); }
}
