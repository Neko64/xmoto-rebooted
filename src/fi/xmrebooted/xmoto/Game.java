package fi.xmrebooted.xmoto;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.opengl.GL11;

import fi.xmrebooted.BuildConfig;
import fi.xmrebooted.xmoto.physics.ode.ODE_Something;

import static org.lwjgl.opengl.GL11.*;


public final class Game {
	
	private static Game s_inst = null;
	
	// TODO: Take care of this somewhere else
	private Window m_window;
	
	public Game() {
		if (null != s_inst) {
			System.err.println();
			return;
		}
		// May not be a good idea...
		s_inst = this;
		
	}
	
	class Point2D {
		float x = 0.0f, y = 0.0f;
	}
	
	public void Run() {
		GLFW_Something.Initialize(); // TODO: Take care of this somewhere else
		
		m_window = new Window();
		if (!m_window.Create(800, 600, "XMoto Rebooted - " + (BuildConfig.kVERSION_MAJOR +
				"." + BuildConfig.kVERSION_MINOR + "." + BuildConfig.kVERSION_PATCH))) {
			/* ... */
		}
		
		GLFW_Something.InitGLCaps(); // TODO: Take care of this somewhere else
		
		// TODO: Factor out all of the GLFW code and
		// move it to some other place
		
		// Hint: 0, 255, 51 (0.0, 1.0, 0.2) is a nice green color
		
		GL11.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		ODE_Something.InitODE();
		//ODE_Something.RunTest();
		
		while (m_window.IsOpen()) {
			float x = 0.0f;
			float y = 0.0f;
			ODE_Something.Integrate();
			
			x = ODE_Something.TEMP_GetSphereX();
			y = ODE_Something.TEMP_GetSphereY();
			
			GL11.glMatrixMode(GL11.GL_MODELVIEW);
			GL11.glLoadIdentity();
			
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
			GL11.glOrtho(0.0f, 800.0f, 0.0f, 600.0f, 0.0f, 1.0f);
			
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
			
			glPushMatrix();
			glMatrixMode(GL11.GL_MODELVIEW);
			glTranslatef(x, y, 0);
			
			RenderCircle(80.0f, 50);
			
			glPopMatrix();
			
			m_window.SwapBuffers();
			glfwPollEvents();
		}
		
		if (!m_window.Close()) {
			System.err.println("Could not close GLFW window");
		}
		
		if (!GLFW_Something.Terminate()) {
			System.err.println("Could not terminate GLFW");
		}
	}
	
	private void RenderCircle(float radius, int numPoints) {
		glBegin(GL_LINES);
		for (int i = 0; i < numPoints - 1; ++i) {
			float angle = (360.0f / (float)numPoints) * (float)i;
			
			float x1 = radius * (float)Math.cos(Math.toRadians(angle));
			float y1 = radius * (float)Math.sin(Math.toRadians(angle));
			
			// Increment the angle by <i>i</i> (it basically makes the angle be
			// the direction of some vector pointing from 0,0 to <i>i</i>
			// point to the next desired point on the line segment around
			// the circle
			angle = (360.0f / (float)numPoints) * (float)(i+1);//Re-calculate ->
			// the angle for the next point in the line segment
			
			float x2 = radius * (float)Math.cos(Math.toRadians(angle));
			float y2 = radius * (float)Math.sin(Math.toRadians(angle));
			
			glVertex2f(x1, y1);
			glVertex2f(x2, y2);
		}
		
		float a = (360.0f / (float)numPoints) * (float)(numPoints-1);
		// Finally, render a line from the last point to the first one
		glVertex2f(radius * (float)Math.cos(Math.toRadians(0.0f)), radius * (float)Math.sin(Math.toRadians(0.0f)));
		glVertex2f(radius * (float)Math.cos(Math.toRadians(a)), radius * (float)Math.sin(Math.toRadians(a)));
		
		glEnd();
		
	}
}
