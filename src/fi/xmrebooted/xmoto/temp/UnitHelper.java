package fi.xmrebooted.xmoto.temp;

public class UnitHelper {
	
	public static final float METERS_TO_PIXELS = 100.0f;
	public static final float PIXELS_TO_METERS = 1.0f / METERS_TO_PIXELS;
	
	public static final float M2P = METERS_TO_PIXELS;
	public static final float P2M = PIXELS_TO_METERS;
	
	public static final float MTP = METERS_TO_PIXELS;
	public static final float PTM = PIXELS_TO_METERS;
}
