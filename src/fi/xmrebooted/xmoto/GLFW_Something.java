package fi.xmrebooted.xmoto;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;

public class GLFW_Something {
	
	/*
	 * TODO: Rename this class
	 * TODO: Redesign the "structure" of this class
	 * TODO: Rename the functions of this class
	 * */
	
	public static boolean Initialize() {
		GLFWErrorCallback.createPrint(System.err).set();
		
		if (!glfwInit())
			throw new IllegalStateException("Could not initialize GLFW");
		
		return true;
	}
	
	public static boolean Terminate() {
		glfwTerminate();
		glfwSetErrorCallback(null).free();
		return true;
	}
	
	public static boolean InitGLCaps() {
		GL.createCapabilities();
		return true;
	}
}
