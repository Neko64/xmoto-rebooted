package fi.xmrebooted.xmoto.physics.ode;

import org.ode4j.ode.DMass;
import org.ode4j.ode.DSimpleSpace;
import org.ode4j.ode.DSphere;
import org.ode4j.ode.DWorld;
import org.ode4j.ode.OdeHelper;

public class ODE_Something {
	
	private static DWorld world;
	
	private static DSimpleSpace space;
	private static DSphere sphere;
	//private static DBox box;
	
	public static void InitODE() {
		OdeHelper.initODE2(0);
		
		world = OdeHelper.createWorld();
		world.setGravity(0.0, -9.81, 0.0);
		
		space = OdeHelper.createSimpleSpace();
		sphere = OdeHelper.createSphere(1);
		//box = OdeHelper.createBox(1, 2, 3);
		
		sphere.setBody(OdeHelper.createBody(world));
		
		DMass m = OdeHelper.createMass();
		m.setSphere(10.0f, sphere.getRadius());
		
		sphere.setPosition(0, 1.0, 0);
		sphere.getBody().setMass(m);
		sphere.getBody().setAngularVel(0.0, 0.0, 0.0);
	}
	
	public static float TEMP_GetSphereX() {
		return (float)sphere.getPosition().get0();
	}
	
	public static float TEMP_GetSphereY() {
		return (float)sphere.getPosition().get1();
	}
	
	public static void Integrate() {
		world.step(1.0 / 60.0);
	}
	
	public static void TerminateODE() {
	}
}
